#
#  This is a beta version of the Rails on Docker project.
#             Created by Amaury (C) 2017
#

# This are the software configuration variables, you can change it as you like.
INSTALLATION_PATH="$HOME/.local/bin/rails_on_docker"
WORKING_DIR="$HOME/code"
# Uncomment this line if you are using different repositories.
GEM_SOURCES="http://172.17.0.1/index/rubygems/rubygems/ http://172.17.0.1/index/rubygems/rubygems-updates/"

# READ!!!!!!
# Application's variables, pease, do not changes this.
PRESENTATION="""
This is the rails on docker project created for...
"""
INSTALLATION_PROGRESS="Installing...done"
RUBY_VERSION="2.4.1"
APP_NAME="rd"

CONTAINER="$(echo $APP_NAME)_$USER"
RUBY_VERSION_FILE="$INSTALLATION_PATH/.ruby_version"
CURRENT_CONTAINER="$(echo $CONTAINER)_$(cat $RUBY_VERSION_FILE 2> /dev/null)"

GROUPID=$(id -g $USER)
USERID=$(id -u $USER)

DONE_MESSAGE="""

Copy this line into ~/.bashrc if needed:

    export PATH=\"$INSTALLATION_PATH:\$PATH\" 

For the first use run this commands:

    $APP_NAME use $RUBY_VERSION
    $APP_NAME create

Rails only works inside $WORKING_DIR directory.

See \`$APP_NAME help\` for more information.

All done!!!
"""
USSAGE="""
Welcome to Rails on Docker (beta) Users guide
"""


if [ "$0" = "$INSTALLATION_PATH/install.sh" ] || [ "$0" = "./install.sh" ];
then    
    rm -r $INSTALLATION_PATH 2> /dev/null; mkdir -p $INSTALLATION_PATH
    echo $RUBY_VERSION > $RUBY_VERSION_FILE
    cp install.sh $INSTALLATION_PATH/$APP_NAME
    
    echo """
$PRESENTATION 
$INSTALLATION_PROGRESS
$DONE_MESSAGE
"""

    for i in "bundle" "gem" "rails" "rake" "ruby" "yard";
    do
        ln -s $INSTALLATION_PATH/$APP_NAME $INSTALLATION_PATH/$i
    done

elif [ "$0" = "$INSTALLATION_PATH/$APP_NAME" ] || [ "$0" = "./$APP_NAME" ];
then
    if [ "$1" = "use" ];
    then
        touch $RUBY_VERSION_FILE
        if [ "$2" ];
        then
            docker stop $CURRENT_CONTAINER 1> /dev/null 2> /dev/null
            echo "changed to ruby:$2"
            echo $2 > $RUBY_VERSION_FILE
        else
            echo "using ruby:$(cat $RUBY_VERSION_FILE)"
        fi
    elif [ "$1" = "help" ];
    then
        echo $USSAGE
    elif [ "$1" = "console" ];
    then
        docker start $CURRENT_CONTAINER 1> /dev/null
        if [ "$2" != "root" ]; then
            RUN_USER="-u $USERID:$GROUPID"
        fi
        docker exec -it $RUN_USER $CURRENT_CONTAINER /bin/bash
    elif [ "$1" = "rm" ];
    then
        docker rm -f $(echo $CONTAINER)_$2
    elif [ "$1" = "create" ];
    then
        COMMAND0="echo fs.inotify.max_user_watches=524288 | tee -a /etc/sysctl.conf && sysctl -p"
        echo "Adding user $USER"
        COMMAND1="groupadd -g $GROUPID $USER &&\
                useradd -u $USERID -g $GROUPID $USER &&\
                chown $USER:$USER $HOME -R"
        
        if [ "$GEM_SOURCES" ]; then 
            ADD_SOURCES="gem sources -r https://rubygems.org/ &&"
        fi
        for source in $GEM_SOURCES; do
            ADD_SOURCES="$ADD_SOURCES gem sources -a $source &&"
        done

        # Deprecated
        EXPORT_PATH="echo export PATH=/usr/local/bundle/bin/:/usr/local/bundle/:/usr/local/bin/:\\\$PATH >> $HOME/.bashrc"

        COMMAND2="$ADD_SOURCES $EXPORT_PATH && gem install rails yard --no-ri --no-rdoc"

        for i in "3000" "3001" "3002" "3003" "3004" "3005" "8808"; do
            PORTS="$PORTS -p 3$i:$i"
        done
        
        IMAGE="ruby"
        VERSION="$(cat $RUBY_VERSION_FILE)"

        docker rm -f $CURRENT_CONTAINER 1> /dev/null 2> /dev/null
        docker run -itd --privileged $PORTS -v $WORKING_DIR:$WORKING_DIR -w $WORKING_DIR -h $APP_NAME-machine --name $CURRENT_CONTAINER $IMAGE:$VERSION bash
        docker start $CURRENT_CONTAINER 1> /dev/null
        docker exec -t $CURRENT_CONTAINER bash -l -c "$COMMAND0"
        docker exec -t $CURRENT_CONTAINER bash -l -c "$COMMAND1"
        docker exec -t -u $USERID:$GROUPID $CURRENT_CONTAINER bash -l -c "$COMMAND2"
    fi
else
    ADDR=$(echo $0 | tr "/" "\n")

    for i in $ADDR; do
        EXEC=$i
    done 
    for i in "rails" "yard"; do
        if [ "$EXEC" = "$i" ]; then
            EXEC="/usr/local/bundle/bin/$EXEC"
        fi
    done

    for i in $@; do
        PARAMETERS="$PARAMETERS $i"
    done

    for i in "server" "s" "console" "c"; do
        if [ "$1" = "$i" ]; then
            I=i
        fi
    done

    COMMAND="cd $PWD && $EXEC $PARAMETERS"

    docker start $CURRENT_CONTAINER 1> /dev/null && \
    docker exec -t$I -u $USERID:$GROUPID $CURRENT_CONTAINER bash -l -c "$COMMAND"

    # Not Working!!!
    if [ "$EXEC" = "rails" ] && [ "$1" = "new" ]; then
        echo """
# You are using Rails On Docker (Beta) 
# Mantainer <amauryuh@gmail.com>
source '$GEM_SOURCES'
$(tail -n +2 $2/Gemfile)
        """ > $2/Gemfile
    fi
fi

